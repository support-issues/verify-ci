# The "verify-ci" repository for support and development collaboration 

This repository provides GitLab support engineers a mechanism to request formal assistance from the GitLab Verify development team. 


## Support User guidance

1. All requests for tickets need to be requested via an issue.
1. Use the issue template "SupportRequestTemplate".
1. Ensure all template fields are completed and all corresponding files are attached.
1. Ensure that severity label is set and corresponds with the priority level in Zendesk.
1. Ensure a link to the corresponding isssue is added to the Zendesk ticket as an internal note.
1. If the Zendesk ticket is escalated then add the "Support::Escalated"

## SLO 

The expected SLO (Labels) for the FRT (First Technical Response) from development are as follows:

- Severity 1 - 12 hours
- Severity 2 - 24 hours
- Severity 3 - 36 hours
- Severtiy 4 - 48 hours

## Development Triage and Priortization

- For guideance on the triage process refer to the handbook [Triage Process](https://about.gitlab.com/handbook/engineering/workflow/#triage-process) 
- For guideance on internal priortization refer to the handbook [Priortization](https://about.gitlab.com/handbook/product/product-processes/#prioritization) 

